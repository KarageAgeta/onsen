<?php
	// 前回の宿番号を取得
	if (isset($_POST['prev_h_id']) && !empty($_POST['prev_h_id'])) {
		$prev_h_id = $_POST['prev_h_id'];
	} else { //ない場合は前回の宿番号を0とする
		$prev_h_id = 0;
	}

	// rand関数で生成した乱数%県名数で、何番目の県名を取得するかを決める
	$prefecture_count = (mt_rand() % 47) + 1;

	// 県コードを成形
	if ($prefecture_count < 10) {
		$prefecture_code = '0'. (string)$prefecture_count. '0000';
	} else {
		$prefecture_code = (string)$prefecture_count. '0000';
	}

	// 温泉有りと県コードをパラメータに設定し、結果のxmlを取得する
	// 取得した県コードと「温泉有り」の条件をパラメータに設定
	$getResultNumParams = array(
			'key' => 'and146230a598e',
			'pref' => $prefecture_code,
			'onsen' => '1'
	);

	// クエリを組み立ててURL生成
	$getResultNumQuery = http_build_query($getResultNumParams, '', '&');
	$getResultNumUrl = 'http://jws.jalan.net/APIAdvance/HotelSearch/V1/?' . $getResultNumQuery;

	// 結果数を取得する
	// xmlファイルの成形
	$getResultNumXml = @simplexml_load_file($getResultNumUrl);

	// 返ってきたxml の中身を読み込む
  	$maxNumStr = $getResultNumXml->NumberOfResults;

	// 乱数%結果数で、先ほどの結果の何番目の宿を取得するかを決定する
  	$maxNum = (int)$maxNumStr;
  	$onsen_count = (mt_rand() % $maxNum) + 1;

	// 温泉有りと県コード表示する数、スタートの位置、写真のサイズをパラメータに設定し、結果のxmlを取得する
  	// 取得した県コードと「温泉有り」の条件とスタート位置、表示する数、写真のサイズをパラメータに設定
  	$getOnsenParams = array(
  			'key' => 'and146230a598e',
  			'pref' => $prefecture_code,
  			'onsen' => '1',
  			'start' => $maxNum,
  			'count' => '1',
  			'pict_size' => '5',
  			'picts' => '5'
  	);

  	// クエリを組み立ててURL生成
  	$getOnsenQuery = http_build_query($getOnsenParams, '', '&');
  	$getOnsenUrl = 'http://jws.jalan.net/APIAdvance/HotelSearch/V1/?' . $getOnsenQuery;

  	// 結果数を取得する
  	// xmlファイルの成形
  	$getOnsenXml = @simplexml_load_file($getOnsenUrl);

  	// 返ってきたxml の中身を読み込む
  	// 結果のxmlから県名、地域、宿名、写真のURL、写真のキャプションを取得する
  	$hotelName = $getOnsenXml->Hotel->HotelName;
  	$hotelPrefecture = $getOnsenXml->Hotel->Area->Prefecture;
  	$hotelArea = $getOnsenXml->Hotel->Area->SmallArea;
  	$hotelPictureURL = $getOnsenXml->Hotel->PictureURL[1];
  	$hotelPictureCaption = $getOnsenXml->Hotel->PictureCaption[1];
  	$hotelPictureURL2 = $getOnsenXml->Hotel->PictureURL[3];
  	$hotelPictureCaption2 = $getOnsenXml->Hotel->PictureCaption[3];
?>

<!docutype html>
<html>
	<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="./css/style.css" type="text/css">
	</head>

	<body id="indexBody">

	<div id="header">
		<a href="./mypage.php" id="mypage">マイページ</a>
		<a href="./newUser.php" id="newUser">新規登録</a>
		<a href="./login.php" id="login">ログイン</a>
	</div>

		<div id="wrapper">

		<div id="onsenPhoto">

			<div id="logo"></div>

			<div id="photo001">
				<img alt=""
				src="<?php echo $hotelPictureURL?>"
				height="450px">
				<div id="photo001Caption">
				<?php echo $hotelPictureCaption?>
				</div>
			</div>

			<div id="photo002">
				<img alt=""
				src="<?php echo $hotelPictureURL2?>"
				height="450px">
				<div id="photo002Caption">
				<?php echo $hotelPictureCaption2?>
				</div>
			</div>
		</div>

			<div id="clip">
			</div>

		<hr id="center">

		<div id="onsenPlace">
		<?php echo $hotelName?> &nbsp; <?php echo $hotelPrefecture?> &nbsp; <?php echo $hotelArea?>
			</div>

			<div id="more">
				<form method="post" action="./index.php">
					<input type="submit" id="moreButton" value="もっと見る">
				</form>
			</div>

				</div>


	</body>
</html>