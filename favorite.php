<?php

// ボタンが押されたときに以下の動作を行う
if (isset($_POST['favorite'])) {

// ログインしていなかった場合にログインページにリダイレクトする
// セッション開始
	session_start();

// セッションの"id"がnullの場合
	if (!isset($_SESSION['id']) && !empty($_SESSION['id'])) {

// セッション"previous_url"に"favorite.php"を入れる
		$_SESSION['previous_url'] = '../favorite.php';

// セッション"favorite_hotel"に連想配列"hotel"を入れる
// ログインページにリダイレクトする
		header('Location: ./LoginTest/login.php');
		exit();

	} else { // セッションの"id"がnullでない場合

// ユーザ名とhotel_idをテーブル"favorite"に登録する
		$sql = "INSERT INTO favorite (
				no,
				id,
				hotel_id
				)
				 VALUES(
				'',
				'".$_SESSION['id']."',
				'".$_SESSION['favorite']."'
				);";

// DBに接続するためのファイルを読み込む
		require_once ("./DB.php");

// インスタンス生成
		$DBConnection = Dao::dbConnection();

// INSERT文を発行し、ユーザ名とhotel_idを発行する
		try {
			$DBpps = $DBConnection->prepare($sql);
			$DBpps->execute();
		} catch (PDOException $e) {
			die("select命令発行時エラー");
		}
	}

	// ログインページにリダイレクトする
	header('Location: ./LoginTest/mypage.php');
	exit();

}


?>