<!docutype html>
<html>

<head>
<meta charset="utf-8">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" href="../css/mypageStyle.css" type="text/css">
<link rel="shortcut icon" href="../img/favicon.ico" />
<title>温泉 - ログイン</title>
</head>

<body id="loginBody">

	<div id="header">
		<div id="logo"></div>
		<span class="yellow">◆</span>
		<a href="./mypage.php" id="mypage">マイページ</a>
		<span class="yellow">◆</span>
		<a href="./newUser.php" id="newUser">新規登録</a>
		<span class="yellow">◆</span>
		<a href="./login.php" id="login">ログイン</a>
		<span class="yellow">◆</span>
	</div>

	<div id="login">
		<table id="loginTable">
			<form method="POST" action="./login.php">
				<tr>
					<td>
						ユーザ名
					</td>
					<td>
						<input type="text" name="id">
					</td>
				</tr>

				<tr>
					<td>
						パスワード
					</td>
					<td>
						<input type="password" name="pass">
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="ログイン" name="login" />
					</td>
			</tr>
			</form>
		</table>
	</div>
</body>

</html>

<?php

if (isset($_POST['login'])) {

	if (isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['pass']) && !empty($_POST['pass'])) {

		$userID = $_POST['id'];
		$userPass = $_POST['pass'];

// ユーザIDと一致する情報を探索する
		$sql = "SELECT * FROM users WHERE id = '" .$userID ."';";

// DBに接続するためのファイルを読み込む
		require_once ("./DB.php");

// インスタンス生成
		$DBConnection = Dao::dbConnection();

// SQL文の実行（ユーザ名からパスワードを検索）
	try {
		$DBpps = $DBConnection->prepare($sql);
		$DBpps->execute();
	} catch (PDOException $e) {
		die("select命令発行時エラー");
	}

// 問い合わせ結果を取得
	$result = $DBpps->fetch(PDO::FETCH_NAMED);

// パスワードが一致するかを調べる
		if ((string)$result['pass'] == (string)$userPass ) {
			mysqli_close($DBLink);

// セッション開始
			session_start();

			$_SESSION['id'] = $userID;
			$_SESSION['time'] = time();

			header('Location: ./mypage.php');
			exit();

			} else {

			print ('ユーザ名とパスワードが一致しません。');
		}
	} else {
		print ('ユーザ名とパスワードを入力してください。');
	}
}

?>