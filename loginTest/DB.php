<?php

class Dao {
// 使用するDBの情報
	const DBUrl = "mysql:dbname=onsen; host=localhost";
	const DBUser = "root";
	const DBPass = "";

// コネクションを再利用できるように静的プロパティにする
	private static $DBConnection = null;

// コンストラクタ、生成時に実行
	public function __construct() {
		Dao::$DBConnection = Dao::dbConnection();
	}

// デストラクタ、破棄時に実行
	public function __destruct() {
		Dao::$DBConnection = null;
	}

	public static  function dbConnection() {
// MySQLへ接続する
		if (Dao::$DBConnection == null) {
			try {
				Dao::$DBConnection = new PDO(Dao::DBUrl, Dao::DBUser, Dao::DBPass);
				Dao::$DBConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				Dao::$DBConnection->exec("set names utf8");
			} catch (PDOException $e) {
				die("接続時エラー");
			}
		}
		return Dao::$DBConnection;
	}
}

?>