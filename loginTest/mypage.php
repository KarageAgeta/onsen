<?php
// セッション開始
session_start();

if (!isset($_SESSION['id']) || empty($_SESSION['id'])) {
	header('Location: ./login.php');
} else {
	$userID = $_SESSION['id'];

// ユーザIDと一致する情報を探索する
	$sql = "SELECT * FROM users WHERE id = '" .$userID ."';";

// DBに接続するためのファイルを読み込む
	require_once ("./DB.php");

// インスタンス生成
	$DBConnection = Dao::dbConnection();

// SQL文の実行（ユーザ名からパスワードを検索）
	try {
		$DBpps = $DBConnection->prepare($sql);
		$DBpps->execute();
	} catch (PDOException $e) {
		die("select命令発行時エラー");
	}

// 問い合わせ結果を取得
	$result = $DBpps->fetch(PDO::FETCH_NAMED);

// ユーザプロフィール情報
	$username = $result['username'];
	$userIcon = $result['icon_url'];

// ユーザIDと一致する情報を探索する
	$sql = "SELECT hotel_id FROM favorite WHERE id = '" .$userID ."';";

// 「行きたい！」一覧を表示する
// SQL文の実行（ユーザ名からパスワードを検索）
	try {
		$DBpps = $DBConnection->prepare($sql);
		$DBpps->execute();
	} catch (PDOException $e) {
		die("select命令発行時エラー");
	}

	// 問い合わせ結果を取得
	$favorite_result = $DBpps->fetch(PDO::FETCH_NAMED);

// 一つのhotel_idごとにじゃらんのAPIを叩く
// 温泉有りと県コード表示する数、スタートの位置、写真のサイズをパラメータに設定し、結果のxmlを取得する
// 取得した県コードと「温泉有り」の条件とスタート位置、表示する数、写真のサイズをパラメータに設定
		$getOnsenParams = array(
				'key' => 'and146230a598e',
				'h_id' => $favorite_result['hotel_id'],
				'xml_ptn' => '2'
				);

// クエリを組み立ててURL生成
		$getOnsenQuery = http_build_query($getOnsenParams, '', '&');
		$getOnsenUrl = 'http://jws.jalan.net/APIAdvance/HotelSearch/V1/?' . $getOnsenQuery;

// 結果数を取得する
// xmlファイルの成形
		$getOnsenXml = @simplexml_load_file($getOnsenUrl);
		$Hotel = $getOnsenXml->Hotel; // 宿の情報
		$Plan = $Hotel->Plan; // プランの情報

		// 結果のxmlから県名、地域、宿名、写真のURL、写真のキャプションを取得する
		$hotel_id = (string)$Hotel->HotelID;
		$hotel_name = (string)$Hotel->HotelName;
		$hotel_prefecture = (string)$Hotel->Area->Prefecture;
		$hotel_area = (string)$Hotel->Area->SmallArea;
		$url = (string)$Hotel->HotelDetailURL;
		$plan_name_001 = (string)$Plan->PlanName[0];
		$plan_url_001 = (string)$Plan->PlanDetailURL[0];
		$plan_name_002 = (string)$Plan->PlanName[1];
		$plan_url_002 = (string)$Plan->PlanDetailURL[1];
		$plan_name_003 = (string)$Plan->PlanName[2];
		$plan_url_003 = (string)$Plan->PlanDetailURL[2];

		// 「行きたい！」に登録する用の連想配列を生成
		$hotel_data = array(
				'hotel_id' => $hotel_id,
				'hotel_name' => $hotel_name,
				'hotel_prefecture' => $hotel_prefecture,
				'hotel_area' => $hotel_area,
				'url' => $url,
				'plan_name_001' => $plan_name_001,
				'plan_url_001' => $plan_url_001,
				'plan_name_002' => $plan_name_002,
				'plan_url_00' => $plan_url_002,
				'plan_name_003' => $plan_name_003,
				'plan_url_003' => $plan_url_003
		);


// ログアウト時の処理
	if (isset($_POST['logout'])) {
		// セッション死
		session_destroy();

// ログインページに戻る
		header('Location: ./login.php');
	}
}

?>

<!docutype html>
<html>

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../css/mypageStyle.css" type="text/css">
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<link rel="shortcut icon" href="../img/favicon.ico" />
	<title>温泉 - マイページ</title>
</head>

<body id="mypageBody">
	<div id="header">
		<div id="logo"></div>
		<span class="yellow">◆</span>
		<a href="./loginTest/mypage.php" id="mypage">マイページ</a>
		<span class="yellow">◆</span>
		<a href="./loginTest/newUser.php" id="newUser">新規登録</a>
		<span class="yellow">◆</span>
		<a href="./loginTest/login.php" id="login">ログイン</a>
		<span class="yellow">◆</span>
		<form method="POST" action="mypage.php" id="logoutButton">
			<input type="submit" value="ログアウト" name="logout">
		</form>
	</div>

	<div id="wrapper">

		<div id="favoriteDisplay">
		<?php echo $username ?>&nbsp;さん<br />
			<div id="title">
			<br />
			「行きたい！」温泉一覧
			</div>
		<table>
			<?php
			echo '<tr>';
			echo '<td>';
			echo $hotel_data['hotel_name'];
			echo '</td>';
			echo '<td>';
			echo $hotel_data['hotel_prefecture'];
			echo '</td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td>';
			echo $hotel_data['plan_name_001'];
			echo '</td>';
			echo '<td>';
			echo $hotel_data['plan_name_002'];
			echo '</td>';
			echo '</tr>';
			?>
		</table>
		</div>
		</div>

	</div>

</body>

</html>
